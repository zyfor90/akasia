<?php

namespace App\Services;

use App\Models\Loan;
use App\Models\ReceivedRepayment;
use App\Models\ScheduledRepayment;
use App\Models\User;

class LoanService
{
    /**
     * Create a Loan
     *
     * @param  User  $user
     * @param  int  $amount
     * @param  string  $currencyCode
     * @param  int  $terms
     * @param  string  $processedAt
     *
     * @return Loan
     */
    public function createLoan(User $user, int $amount, string $currencyCode, int $terms, string $processedAt): Loan
    {
        // * use DB Transaction to avoid Inconsistent Data
        return \DB::transaction(function() use ($user, $amount, $currencyCode, $terms, $processedAt) {
            // * Create loan
            $loan = Loan::create([
                'user_id' => $user->id,
                'amount' => $amount,
                'terms' => $terms,
                'outstanding_amount' => $amount,
                'currency_code' => $currencyCode,
                'processed_at' => $processedAt,
                'status' => Loan::STATUS_DUE,
            ]);

            // * generate dueDate with start date (processedAt)
            $dueDate = date('Y-m-d', strtotime("+1 month", strtotime($processedAt)));

            $count = 0;
            for ($i = 0; $i < $terms; $i++) {
                // split the amount into several piece
                $repaymentAmount = floor($amount / $terms);
                if ($i+1 == $terms) {
                    $repaymentAmount = round($amount - $count);
                } else {
                    $count += floor($amount / $terms);
                }

                // * Create Scheduled Repayment
                ScheduledRepayment::create([
                    'loan_id' => $loan->id,
                    'amount' => (int) $repaymentAmount,
                    'outstanding_amount' => (int) $repaymentAmount,
                    'currency_code' => $currencyCode,
                    'due_date' => $dueDate,
                    'status' => ScheduledRepayment::STATUS_DUE,
                ]);

                // * Overide duedate and add +1 month
                $dueDate = date('Y-m-d', strtotime("+1 month", strtotime($dueDate)));
            }

            return $loan;
        });
    }

    /**
     * Repay Scheduled Repayments for a Loan
     *
     * @param  Loan  $loan
     * @param  int  $amount
     * @param  string  $currencyCode
     * @param  string  $receivedAt
     *
     * @return ReceivedRepayment
     */
    public function repayLoan(Loan $loan, int $amount, string $currencyCode, string $receivedAt): ReceivedRepayment
    {
        // * use DB Transaction to avoid Inconsistent Data
        return \DB::transaction(function() use ($loan, $amount, $currencyCode, $receivedAt) {

            // * Update outstanding amount first
            $scheduls = ScheduledRepayment::where('loan_id', $loan->id)->where('outstanding_amount', 0)->where('status', ScheduledRepayment::STATUS_DUE)->get();
            foreach ($scheduls as $schedul) {
                $schedul->update([
                    'outstanding_amount' => $schedul->amount
                ]);
            }

            // * Duplicate amount value to paid variable for amount on table received_repayments
            $paid = $amount;

            // * Get on going repayment and pay it
            $dueRepayments = ScheduledRepayment::where('loan_id', $loan->id)->where('status', ScheduledRepayment::STATUS_DUE)->orWhere('status', ScheduledRepayment::STATUS_PARTIAL)->get();
            foreach ($dueRepayments as $dueRepayment) {

                // * count the repayment amount
                $repaymentAmount = min($dueRepayment->outstanding_amount, $amount);

                // * count total after you paid it
                $totalRepay = max(0, $dueRepayment->outstanding_amount - $repaymentAmount);

                // * Update SceduleRepayment and change outsanding amount
                $dueRepayment->update([
                    'outstanding_amount' => $totalRepay,
                    'status' => ($totalRepay == 0) ? ScheduledRepayment::STATUS_REPAID : ScheduledRepayment::STATUS_PARTIAL,
                ]);

                // * decrease the current amount because the repayment amount already paid it.
                $amount -= $repaymentAmount;

                // Break the loop if the received amount is fully used
                if ($amount == 0) {
                    break;
                }
            }

            // Create a new received repayment record
            $receivedRepayment = ReceivedRepayment::create([
                'loan_id' => $loan->id,
                'amount' => $paid,
                'currency_code' => $currencyCode,
                'received_at' => $receivedAt,
            ]);

            // * Get outstanding amount on the scheduled_repayments table and sum it.
            $totalOutstandingAmount = ScheduledRepayment::where('loan_id', $loan->id)->sum('outstanding_amount');

            // * Update current outstanding loan amount with the new one.
            $loan->update([
                'outstanding_amount' => $totalOutstandingAmount,
                'status' => $totalOutstandingAmount == 0 ? Loan::STATUS_REPAID : Loan::STATUS_DUE,
            ]);

            return $receivedRepayment;
        });
    }
}
