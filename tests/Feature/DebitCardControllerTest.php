<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

use App\Models\User;
use App\Models\DebitCard;
use App\Models\DebitCardTransaction;

class DebitCardControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        Passport::actingAs($this->user);
    }

    public function testCustomerCanSeeAListOfDebitCards()
    {
        // get /debit-cards

        // * Create User
        DebitCard::factory(30)->create(['user_id' => $this->user->id, 'disabled_at' => null]);

        $response = $this->get('/api/debit-cards');
        $response->assertJsonStructure([
            '*' => [
                'id',
                'number',
                'type',
                'expiration_date',
                'is_active'
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testCustomerCannotSeeAListOfDebitCards()
    {
        // get /debit-cards

        // * Create User
        DebitCard::factory(30)->create(['user_id' => $this->user->id, 'disabled_at' => now()]);

        $response = $this->get('/api/debit-cards');
        $response->assertJsonStructure([
            '*' => [
                'id',
                'number',
                'type',
                'expiration_date',
                'is_active'
            ]
        ]);
        $response->assertStatus(200);
    }

    public function testCustomerCannotSeeAListOfDebitCardsOfOtherCustomers()
    {
        // get /debit-cards

        $otherCustomer = User::factory()->create();
        DebitCard::factory(30)->create(['user_id' => $otherCustomer->id]);

        $response = $this->get('/api/debit-cards');

        $response->assertExactJson([]);    // Check if the response is an empty JSON array
        $response->assertStatus(200);
    }

    public function testCustomerCanCreateADebitCard()
    {
        // post /debit-cards
        $response = $this->post('/api/debit-cards', ['type' => 'visa']);

        $response->assertJsonStructure([
                'id',
                'number',
                'type',
                'expiration_date',
                'is_active'
        ]);

        $response->assertJsonFragment(['type' => 'visa']);
        $response->assertJsonFragment(['is_active' => true]);
        $response->assertStatus(201);

        $this->assertDatabaseHas('debit_cards', ['type' => 'visa']);
    }

    public function testCustomerCannotCreateADebitCardWithWrongValidation()
    {
        // post /debit-cards
        $response = $this->post('/api/debit-cards');
        $response->assertStatus(302);

        $response = $this->post('/api/debit-cards', ['type' => 123]);
        $response->assertStatus(302);
    }

    public function testCustomerCanSeeASingleDebitCardDetails()
    {
        // get api/debit-cards/{debitCard}
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id, 'disabled_at' => null]);

        $response = $this->get("/api/debit-cards/{$debitCard->id}");
        $response->assertJsonFragment(['id' => $debitCard->id]);
        $response->assertStatus(200);
    }

    public function testCustomerCannotSeeASingleDebitCardDetails()
    {
        // * Create 30 Data
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id, 'disabled_at' => now()]);

        $response = $this->get("/api/debit-cards/{$debitCard->id}");

        $response->assertStatus(200);
    }

    public function testCustomerCanActivateADebitCard()
    {
        // put api/debit-cards/{debitCard}
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id, 'disabled_at' => null]);

        $response = $this->put("/api/debit-cards/{$debitCard->id}", ['is_active' => true]);
        $response->assertJsonStructure([
            'id',
            'number',
            'type',
            'expiration_date',
            'is_active'
        ]);
        $response->assertJsonFragment(['is_active' => true]);
        $response->assertStatus(200);

        $this->assertDatabaseHas('debit_cards', [
            'id' => $debitCard->id,
            'number' => $debitCard->number,
            'type' => $debitCard->type,
            'disabled_at' => $debitCard->disabled_at
        ]);
    }

    public function testCustomerCanDeactivateADebitCard()
    {
        // put api/debit-cards/{debitCard}
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id, 'disabled_at' => null]);

        $response = $this->put("/api/debit-cards/{$debitCard->id}", ['is_active' => false]);

        $response->assertJsonFragment(['is_active' => false]);
        $response->assertStatus(200);

        $updatedDebitCard = DebitCard::where('id', $debitCard->id)->first();
        $this->assertDatabaseHas('debit_cards', [
            'id' => $updatedDebitCard->id,
            'number' => $updatedDebitCard->number,
            'type' => $updatedDebitCard->type,
            'disabled_at' => $updatedDebitCard->disabled_at
        ]);
    }

    public function testCustomerCannotUpdateADebitCardOfOtherCustomers()
    {
        // put api/debit-cards/{debitCard}
        $otherCustomer = User::factory()->create();
        $debitCard = DebitCard::factory()->create(['user_id' => $otherCustomer->id, 'disabled_at' => null]);

        $response = $this->put("/api/debit-cards/{$debitCard->id}", ['is_active' => false]);
        $response->assertStatus(403);
    }

    public function testCustomerCannotUpdateADebitCardWithWrongValidation()
    {
        // put api/debit-cards/{debitCard}
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id]);

        // post /debit-cards
        $response = $this->put("/api/debit-cards/{$debitCard->id}");
        $response->assertStatus(302);

        $response = $this->put("/api/debit-cards/{$debitCard->id}", ['is_active' => 123]);
        $response->assertStatus(302);

        $response = $this->put("/api/debit-cards/{$debitCard->id}", ['is_active' => "string"]);
        $response->assertStatus(302);
    }

    public function testCustomerCanDeleteADebitCard()
    {
        // delete api/debit-cards/{debitCard}
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id, 'disabled_at' => null]);

        $response = $this->delete("/api/debit-cards/{$debitCard->id}");

        $this->assertSoftDeleted('debit_cards', ['id' => $debitCard->id]);

        $response->assertStatus(204);
    }

    public function testCustomerCannotDeleteADebitCardOfOtherCustomers()
    {
        // delete api/debit-cards/{debitCard}
        $otherCustomer = User::factory()->create();
        $debitCard = DebitCard::factory()->create(['user_id' => $otherCustomer->id, 'disabled_at' => null]);

        $response = $this->delete("/api/debit-cards/{$debitCard->id}");

        // check debit card in the database
        $this->assertDatabaseHas('debit_cards', ['id' => $debitCard->id]);

        $response->assertStatus(403);
    }


    public function testCustomerCannotDeleteADebitCardWithTransaction()
    {
        // delete api/debit-cards/{debitCard}
        $debitCard = DebitCard::factory()->create(['user_id' => $this->user->id, 'disabled_at' => null]);
        $debitCard = DebitCardTransaction::factory()->create([
            'debit_card_id' => $debitCard->id
        ]);

        $response = $this->delete("/api/debit-cards/{$debitCard->id}");

        // check debit card in the database
        $this->assertDatabaseHas('debit_cards', ['id' => $debitCard->id]);

        // Ocheck debit transaction in the database
        $this->assertDatabaseHas('debit_card_transactions', ['debit_card_id' => $debitCard->id]);

        $response->assertStatus(403);
    }

    // Extra bonus for extra tests :)
    // I did extra tests I wish I get extra bonus :)
}
