<?php

namespace Tests\Feature;

use App\Models\DebitCard;
use App\Models\DebitCardTransaction;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class DebitCardTransactionControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;
    protected DebitCard $debitCard;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->debitCard = DebitCard::factory()->create([
            'user_id' => $this->user->id
        ]);
        Passport::actingAs($this->user);
    }

    public function testCustomerCanSeeAListOfDebitCardTransactions()
    {
        // get /debit-card-transactions
        DebitCardTransaction::factory(30)->create(['debit_card_id' => $this->debitCard->id]);

        $response = $this->get("/api/debit-card-transactions?debit_card_id={$this->debitCard->id}");
        $response->assertJsonStructure([
            '*' => [
                'amount',
                'currency_code',
            ]
        ]);

        $response->assertStatus(200);
    }

    public function testCustomerCannotSeeAListOfDebitCardTransactionsOfOtherCustomerDebitCard()
    {
        // get /debit-card-transactions
        $otherCustomer = User::factory()->create();
        $otherDebitCard = DebitCard::factory()->create(['user_id' => $otherCustomer->id, 'disabled_at' => null]);
        DebitCardTransaction::factory(30)->create(['debit_card_id' => $otherDebitCard->id]);


        $response = $this->get("/api/debit-card-transactions?debit_card_id={$otherDebitCard->id}");

        $response->assertStatus(403);
    }

    public function testCustomerCanCreateADebitCardTransaction()
    {
        $data = [
            'debit_card_id' => $this->debitCard->id,
            'amount' => 100,
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];

        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        unset($data['debit_card_id']);
        $response->assertJson($data);
        $response->assertStatus(201);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithEmptyData()
    {
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", []);
        $response->assertStatus(403);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithoutSendDebitCardIdOnRequest()
    {
        $data = [
            'amount' => 100,
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(403);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithoutSendAmountOnRequest()
    {
        $data = [
            'debit_card_id' => $this->debitCard->id,
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(302);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithoutSendCurrencyIdrOnRequest()
    {
        $data = [
            'debit_card_id' => $this->debitCard->id,
            'amount' => 100,
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(302);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithIncorrectDebitCardId()
    {
        $data = [
            'debit_card_id' => 100,
            'amount' => 100,
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(403);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithStringDebitCardId()
    {
        $data = [
            'debit_card_id' => '100',
            'amount' => 100,
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(403);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithStringAmount()
    {
        $data = [
            'debit_card_id' => $this->debitCard->id,
            'amount' => 'string',
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(302);
    }

    public function testCustomerCannotCreateADebitCardTransactionWithIncorrectCurrencyCode()
    {
        $data = [
            'debit_card_id' => $this->debitCard->id,
            'amount' => 100,
            'currency_code' => 'USD',
        ];
        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(302);
    }

    public function testCustomerCannotCreateADebitCardTransactionToOtherCustomerDebitCard()
    {
        // post /debit-card-transactions
        $otherCustomer = User::factory()->create();
        $otherDebitCard = DebitCard::factory()->create(['user_id' => $otherCustomer->id, 'disabled_at' => null]);
        $data = [
            'debit_card_id' => $otherDebitCard->id,
            'amount' => 100,
            'currency_code' => DebitCardTransaction::CURRENCY_IDR,
        ];

        // post /debit-card-transactions
        $response = $this->post("/api/debit-card-transactions", $data);
        $response->assertStatus(403);
    }

    public function testCustomerCanSeeADebitCardTransaction()
    {
        // get /debit-card-transactions/{debitCardTransaction}
        $transaction = DebitCardTransaction::factory()->create(['debit_card_id' => $this->debitCard->id]);

        $response = $this->get("/api/debit-card-transactions/{$transaction->id}");
        $response->assertJsonStructure([
            'amount',
            'currency_code',
        ]);

        $response->assertStatus(200);
    }

    public function testCustomerCannotSeeADebitCardTransactionAttachedToOtherCustomerDebitCard()
    {
        // get /debit-card-transactions/{debitCardTransaction}
        $otherCustomer = User::factory()->create();
        $otherDebitCard = DebitCard::factory()->create(['user_id' => $otherCustomer->id]);
        $otherTransaction = DebitCardTransaction::factory()->create(['debit_card_id' => $otherDebitCard->id]);

        $response = $this->get("/api/debit-card-transactions/{$otherTransaction->id}");

        $response->assertStatus(403);
    }

    // Extra bonus for extra tests :)
    // I did extra tests I wish I get extra bonus :)
}
