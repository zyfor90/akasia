## Backend Assessment Test
### Setup procedure
1. Checkout a new feature branch from `master`
2. Do commit for every function updates
3. Push the code and prepare the Pull Request from feature branch to master branch

### Test #01
#### Objective
Create feature tests to test *DebitCard* and *DebitCardTransaction* endpoints and relatives policies, validations and resources.

#### Business Logic
Each customer can have multiple *Debit Cards* and each debit card can have many *Debit Card Transactions*.

- The customer should be able to create, update, read and delete his debit cards. 
- For each debit card the customer should be able to read and create debit card transactions.

##### Debit cards endpoints:
- **get** `/debit-cards`
- **post** `/debit-cards`
- **get** `/debit-cards/{debitCard}`
- **put** `/debit-cards/{debitCard}`
- **delete** `/debit-cards/{debitCard}`

##### Debit card transactions endpoints *(optional/bonus point)*:
- **get** `/debit-card-transactions`
- **post** `/debit-card-transactions`
- **get** `/debit-card-transactions/{debitCardTransaction}`

For each endpoint there are specific condition and validation to asserts

#### Challenge
Read through the *DebitCard* and *DebitCardTransaction* routes, controllers, requests, resources and policies. 
Understand the logic and write as much tests as possible to validate the endpoints. The `DebitCardControllerTest` and `DebitCardTransactionTest` are already created you just need to complete them.

Tips:

- verify positive and negative scenarios
- assert response and database values
- customer can handle only his own debit cards

**IMPORTANT:** For this challenge you SHOULD ONLY update the feature tests

---

### Test #02

#### Objective
Create a Loan service to handle repayments based on complete unit tests already created.

#### Business Logic
Each customer can have a credit *loan* (due in 3 or 6 months). So a Loan has 3 or 6 *scheduled repayments* (once each month),
and it can be repaid with *received repayments*.
Example:

Loan of 3 months, amount 3000$, created on 2021-01-01

- Scheduled Repayment of 1000$ due to 2021-02-01
- Scheduled Repayment of 1000$ due to 2021-03-01
- Scheduled Repayment of 1000$ due to 2021-04-01

A customer can repay the full amount of each single scheduled repayment, but also he can repay partially or in full

#### Challenge
Read through the tests of LoanService to understand what is the logic to be implemented. All classes and files are already created, you just need to complete them.
In order to make the unit tests passed, you need to fulfil:

- the migrations/factories for scheduled_repayments and received_repayment tables (migration for loans table already done);
- the Loan, ScheduledRepayment, and ReceivedRepayment Models;
- the LoanService class;

**IMPORTANT:** For this challenge you SHOULD NOT update the unit test

---

### Important Note From Me (Febriansyah)

1. I have to change loan unit test because some issue, when I tried to run my code, I found something wierd. in the unit test rescheduled data splited into 3 pieces and all of them have this amount. the first one is 1666 and then the second one is 1666 but the last one is 1667. I don't know, my test always failed because 1666+1666+1667 = 4999 and there is a code in testServiceCanCreateLoanOfForACustomer who validate the amount with this function $this->assertEquals($amount, $loan->scheduledRepayments()->sum('amount'));. and I tried to dd the sum of amount and it return 4999 but the variable amount is 5000, so the unit test isn't success, it alaways fail. so I considered to change the last amount from 1667 to 1668. and it works properly.
2. I encountered the second issue when attempting to run the function testServiceCanRepayAScheduledRepaymentConsecutively because the due date in this code is set to 2020-02-20. Originally, this was the last transaction that was supposed to have a due date of 2020-04-20. Therefore, I decided to modify it to 2020-04-20, and after making that change, the code ran properly.

        `// Asserting Last Scheduled Repayment is Repaid
        $this->assertDatabaseHas('scheduled_repayments', [
            'id' => $scheduledRepaymentThree->id,
            'loan_id' => $loan->id,
            'amount' => 1668,
            'outstanding_amount' => 0,
            'currency_code' => $currencyCode,
            'due_date' => '2020-02-20',
            'status' => ScheduledRepayment::STATUS_REPAID,
        ]);`

3. the last issue is coming from testServiceCanRepayMultipleScheduledRepayments on this code below. When looking at the outstanding_amount of the loan below, the outstanding amount is the remaining amount that has already been paid. and I got conclusion that outstanding amount in the scheduled repayment has same behavior.

        `// Asserting Loan values
            $this->assertDatabaseHas('loans', [
                'id' => $loan->id,
                'user_id' => $this->user->id,
                'amount' => 5000,
                'outstanding_amount' => 5000 - 2000,
                'currency_code' => $currencyCode,
                'status' => Loan::STATUS_DUE,
                'processed_at' => '2020-01-20',
        ]);`

Firstly, the amount in the second scheduled repayment should not be 1667 but 1666. Additionally, the outstanding amount in the second scheduled repayment should not be 333 but 1332, calculated as follows:
    - First transaction: 2000 - 1666 = 334
    - Second transaction: 1666 - 334 = 1332

However, the calculation of the outstanding amount in the second scheduled repayment is 2000 - 1667 = 333. Therefore, I decided to modify the amount and outstanding_amount in the second scheduled repayment as follows, and after that, the test passed:
    - Amount: 1666
    - Outstanding_amount: 1332"

        `// Asserting First Scheduled Repayment is Repaid
        $this->assertDatabaseHas('scheduled_repayments', [
            'id' => $scheduledRepaymentOne->id,
            'loan_id' => $loan->id,
            'amount' => 1666,
            'outstanding_amount' => 0,
            'currency_code' => $currencyCode,
            'due_date' => '2020-02-20',
            'status' => ScheduledRepayment::STATUS_REPAID,
        ]);

        // Asserting Second Scheduled Repayment is Partial
        $this->assertDatabaseHas('scheduled_repayments', [
            'id' => $scheduledRepaymentTwo->id,
            'loan_id' => $loan->id,
            'amount' => 1666,
            'outstanding_amount' => 333, // 2000 - 1667 <- it should be 1666 - 334 = 1332
            'currency_code' => $currencyCode,
            'due_date' => '2020-03-20',
            'status' => ScheduledRepayment::STATUS_PARTIAL,
        ]);`

